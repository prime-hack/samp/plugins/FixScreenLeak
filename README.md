[![pipeline  status](https://gitlab.com/prime-hack/samp/plugins/FixScreenLeak/badges/main/pipeline.svg)](https://gitlab.com/prime-hack/samp/plugins/FixScreenLeak/-/commits/main)

# FixScreenLeak

#### Plugin fix memory leak on take screenshot

## Supported versions

#### 0.3.7 R1, 0.3.7 R3

## [Download](https://gitlab.com/prime-hack/samp/plugins/FixScreenLeak/-/jobs/artifacts/main/download\?job\=win32)


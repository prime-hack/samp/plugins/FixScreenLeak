#ifndef FIXSCREENLEAK_LIBRARY_H
#define FIXSCREENLEAK_LIBRARY_H

#include <SRHook.hpp>

class FixScreenLeak {
	SRHook::Hook<> takeScreenShot{ 0x74FC1, 5, "samp" };

public:
	FixScreenLeak();
	~FixScreenLeak();

protected:
	static void removeSurface( SRHook::CPU &cpu );
};

#endif //FIXSCREENLEAK_LIBRARY_H

#include "library.h"
#include <samp_pimpl.hpp>
#include <d3d9.h>

[[maybe_unused]] FixScreenLeak g_instance; // Initialize plugin

FixScreenLeak::FixScreenLeak() {
	if ( SAMP::isR1() ) takeScreenShot.changeAddr( 0x710D1 );
	takeScreenShot.onBefore += &FixScreenLeak::removeSurface;
	takeScreenShot.install( 0, 0, false );
}
FixScreenLeak::~FixScreenLeak() {
	takeScreenShot.remove();
}
void FixScreenLeak::removeSurface( SRHook::CPU &cpu ) {
	auto pSurface = *reinterpret_cast<IDirect3DSurface9 **>( cpu.ESP + 0x10 );
	pSurface->Release();
}
